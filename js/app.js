const select = document.getElementById('breeds');
const card = document.querySelector('.card'); 
const form = document.querySelector('form');

// ------------------------------------------
//  FETCH FUNCTIONS
// ------------------------------------------

//Vamos a simplificar el fetch request centralizandolo
function fetchData(url){
  return fetch(url)
    .then(checkStatus)
    .then(res => res.json()) //parsing response to JSON
    .catch(error => console.log('Looks like there was a problem', error))
}

Promise.all([
  fetchData('https://dog.ceo/api/breeds/list'),
  fetchData('https://dog.ceo/api/breeds/image/random')
])
.then(data => {
  const breedList = data[0].message;
  const randomImage = data[1].message;
  
  generateOptions(breedList);
  generateImage(randomImage);
})


////Con el siguiente fetch recogemos del link del API la raza convertida en formato JSON
//fetchData('https://dog.ceo/api/breeds/list')
//  //.then(response => response.json())
//  .then(data => generateOptions(data.message))
//  //.catch(error => console.log('Looks like there was a problem', error))
//
////Con el siguiente fetch recogemos del link del API la imagen convertida en formato JSON
//fetchData('https://dog.ceo/api/breeds/image/random')
//  //.then(response => response.json())
//.then(data => generateImage(data.message))


// ------------------------------------------
//  HELPER FUNCTIONS
// ------------------------------------------

//this function will check if the promise resolved with the response objetc's ok property set to true
function checkStatus(response){
  if (response.ok){ //if response is true the promise get resolved with the response
    return Promise.resolve(response);
  }else{
    return Promise.reject(new Error(response.statusText));
  }
}

//La siguiente función permite introducir como html la raza obtenida del API y mostrarla como opciones
function generateOptions(data){
  const options = data.map(item => `
    <option value='${item}'>${item}</option>
`).join(''); //Esto permite eliminar el ", " que aparece en el html después del nombre de cada raza
  
  //Ahora introducimos en el html del id breeds (constante select)
  select.innerHTML = options;
}

//La siguiente función permite introducir como html la imagen obtenida del API
function generateImage(data) {
  const html = `
    <img src='${data}' alt>
    <p>Click to view images of ${select.value}s</p>
  `;
  card.innerHTML = html;
}

function fetchBreedImage(){
  const breed = select.value;
  const img = card.querySelector('img');
  const p = card.querySelector('p');
  
  //En vez de pasar solo la url como un string uso un template literal
  //por que quiero interpolar el valor (insertar) de la variable raza en el URL
  fetchData(`https://dog.ceo/api/breed/${breed}/images/random`)
  .then(data => {
    img.src = data.message;
    img.alt = breed;
    p.textContent = `Click to view more ${breed}s`;
  })
}
// ------------------------------------------
//  EVENT LISTENERS
// ------------------------------------------
select.addEventListener('change', fetchBreedImage);
card.addEventListener('click', fetchBreedImage);
form.addEventListener('submit', postData);

// ------------------------------------------
//  POST DATA
// ------------------------------------------

function postData(e){
  e.preventDefault(); // I'm cancelling the default submit behaviour of the server 
  const name = document.getElementById('name').value;
  const comment = document.getElementById('comment').value;
  
  //Para tener mas order creo una constante config que es el 2do parametro de fetch
  const config = {
    method: 'POST',
    headers:{
      'Content-Type': 'application/json' //this communicates to the server that the data has been encoded with JSON
    } ,
    body: JSON.stringify({name: name, comment: comment})//values are send to the server in the body of the request
  }
  
  fetch('https://jsonplaceholder.typicode.com/comments', config)
  .then(checkStatus)
  .then(res => res.json())
  .then(data => console.log(data))
  
}